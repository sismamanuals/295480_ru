﻿.. _impostazioni_applicazione:

#########################
IMPOSTAZIONI APPLICAZIONE
#########################
PRISMA ha una sua pagina di impostazioni nella quale si può modificare la configurazione dell'applicazione.

Nella finestra a tendina selezionare :guilabel:`Strumenti > Impostazioni > Applicazione` e aprire la finestra di gestione delle :guilabel:`IMPOSTAZIONI APPLICAZIONE`.

.. _impostazioni_applicazione_lingua:
.. figure:: _static/impostazioni_applicazione_lingua.png
   :width: 14 cm
   :align: center

   Impostazioni applicazione

******
LINGUA
******
Nella sezione :guilabel:`Lingua` è possibile selezionare la lingua del programma.

Selezionare la lingua desiderata e confermare con il pulsante :kbd:`OK`.

Il pulsante :kbd:`Annulla` permette di chiudere la finestra senza apportare alcuna modifica alla lingua corrente.

********
PERCORSI
********
Nella sezione :guilabel:`Percorsi` vengono configurati gli indirizzi locali da dove PRISMA può accedere al server locale o alle funzionalità di supporto.

Per i dettagli riferirsi alla :numref:`report_e_raccolta_dati`.

*La pagina contiene dei parametri macchina che devono essere compilati e/o modificati solo da un tecnico autorizzato Sisma.*

*************
SISTEMI LASER
*************
PRISMA è strutturato per comunicare con più sistemi laser contemporaneamente, anche se gli scenari normali sono rappresentati da un unico sistema laser connesso ad un solo PC.

*I comandi a disposizione in questa parte sono riservati ad un tecnico autorizzato Sisma.*

***********
INTERFACCIA
***********
In questa sezione si hanno i parametri relativi all'interfaccia grafica. Agendo sulle voci della finestra si posso abilitare o disabilitare le funzioni relative.

Per i dettagli riferirsi alla :numref:`impostazioni_interfaccia`.

*Fatto salvo per alcune azioni indicati nella* :numref:`impostazioni_interfaccia`, *questa finestra è principalmente rivolta ad un utente esperto o ad un tecnico autorizzato Sisma*.

******
UTENTI
******
Questa parte permette di creare un utente personalizzato, andando a definire le attività di sistema a cui è abilitato l'utente.

Per i dettagli riferirsi alla :numref:`impostazioni_utenti`.

*Anche in questo caso, come nel precedente, l'utilizzo della sezione è rivolto ad un tecnico autorizzato Sisma.*

***********
DB COMMESSE
***********
Per i dettagli riferirsi alla :numref:`impostazioni_DB_commesse`.

********
PROFINET
********
Per i dettagli riferirsi alla :numref:`impostazioni_profinet`.
