# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software
# Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: 95dd98c11d15408ca51a17616a5bf90f WIP/295480/295480/source/marcatura.rst:5
msgid "MARCATURA"
msgstr ""

#: WIP/295480/295480/source/marcatura.rst:8 c8af8d8f5c64423883a4bd7225011889
msgid "PRIMA DELLO START"
msgstr ""

#: 64c8440d429d4b0a83ad244b824421ca WIP/295480/295480/source/marcatura.rst:9
msgid ""
"I comandi principali di gestione del laser che si trovano nella barra "
"comandi in alto sono (in ordine di disposizione sulla barra):"
msgstr ""

#: 0a88bb3b54674741bced9b2c3830df97 WIP/295480/295480/source/marcatura.rst:11
msgid ""
":kbd:`Luce` (accende e spegne il faretto interno alla camera di lavoro e "
"permette di ispezionare la camera anche durante la lavorazione);"
msgstr ""

#: 3402acbca20f4221827ee8c1d9096fe8 WIP/295480/295480/source/marcatura.rst:12
msgid ""
":kbd:`Rosso` (attiva la proiezione del puntatore rosso per visualizzare "
"l'area di marcatura e permettere la centratura del pezzo);"
msgstr ""

#: 758fcfc8e78a4e8e90b3baa6c660e9d2 WIP/295480/295480/source/marcatura.rst:13
msgid ":kbd:`Avvia Marcatura` (è il tasto che abilita la marcatura);"
msgstr ""

#: WIP/295480/295480/source/marcatura.rst:14 f9f79f3213554e7f867dd08aaa873694
msgid ":kbd:`Ferma Operazioni` (ferma qualsiasi attività di marcatura in corso);"
msgstr ""

#: WIP/295480/295480/source/marcatura.rst:15 f236262cb20343f5b1a3090905bd6729
msgid ":kbd:`Anteprima di Marcatura` (visualizza l'anteprima della marcatura);"
msgstr ""

#: 3fd2ac2194de40d5b052a79d6cddbdf6 WIP/295480/295480/source/marcatura.rst:16
msgid ""
":kbd:`Stima Tempi` (premendo sull'icona il laser apre nel programma di "
"editor una finestra per stimare i tempi di lavorazione)."
msgstr ""

#: 1c0664ec48a244a8801df3fd16130667 WIP/295480/295480/source/marcatura.rst:23
msgid ".. image:: _static/marcatura_comandi_laser.png"
msgstr ""

#: 43bf241ac76d4072b20d48b0fd148210 WIP/295480/295480/source/marcatura.rst:23
msgid "Comandi di gestione laser"
msgstr ""

#: 4cc7752dafb240b58527c712f29a7728 WIP/295480/295480/source/marcatura.rst:27
msgid "AVVIO MARCATURA LASER"
msgstr ""

#: 11bc4e2a22af44ffa96b632f2173a613 WIP/295480/295480/source/marcatura.rst:28
msgid "Premere :kbd:`AVVIA ROSSO`;"
msgstr ""

#: 5865abedd9014a4daa2d9224eb581320 WIP/295480/295480/source/marcatura.rst:29
msgid ""
"Posizionare il materiale all'interno della camera, centrando il rosso "
"sull'area interessata alla marcatura;"
msgstr ""

#: WIP/295480/295480/source/marcatura.rst:30 dc4301c6879047d9abb351fedd4a974b
msgid "Chiudere il portellone del laser;"
msgstr ""

#: WIP/295480/295480/source/marcatura.rst:31 f21102efc65a4e378c21185c2ea042ad
msgid "Premere :kbd:`AVVIA LASER`"
msgstr ""

#: 8a6ff6db4f6b45c2af3a902ab577cb0b WIP/295480/295480/source/marcatura.rst:33
msgid ""
"Attendere che il laser termini la lavorazione prima di aprire lo "
"sportello."
msgstr ""

#~ msgid ".. image:: _static/Immagine_it_comandi_laser.png"
#~ msgstr ""

